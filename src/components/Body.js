import React from "react";
import Note from "./Note";

export default class Body extends React.Component {
  render() {
    return (
      <div>
        {this.props.text}
        <div>
          <Note text="Vou continuar praticando!" feeling={this.props.feeling} />
        </div>
      </div>
    );
  }
}
