import React from "react";
import FooterNote from "./FooterNote";

export default class Footer extends React.Component {
  render() {
    return (
      <div>
        {this.props.text}
        <div>
          <FooterNote text="Repetir mais!" feeling={this.props.feeling} />
        </div>
      </div>
    );
  }
}
