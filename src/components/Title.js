import React from "react";
import TitleNote from "./TitleNote";

export default class Title extends React.Component {
  render() {
    return (
      <h1>
        {this.props.text}
        <div>
          <TitleNote text="Repetir mais ainda!" feeling={this.props.feeling} />
        </div>
      </h1>
    );
  }
}
